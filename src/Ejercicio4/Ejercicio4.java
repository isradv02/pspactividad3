package Ejercicio4;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        String command = "java -jar Translator.jar";
        String ficheroTraducir = null;

        if(args.length>0){
            List<String> arguments = Arrays.asList(args.clone());
            if(arguments.contains("-d")){
                int indexDicc = arguments.indexOf("-d");
                command = "java -jar Translator.jar -d "+arguments.get(indexDicc+1);
            }
            if(arguments.contains("-f")){
                int indexDicc = arguments.indexOf("-f");
                ficheroTraducir = arguments.get(indexDicc+1);
            }
        }
        try {
            ProcessBuilder pb = new ProcessBuilder(command.split(" "));
            Process pr = pb.start();
            BufferedWriter bw = new BufferedWriter(new FileWriter("Translations.txt"));
            BufferedWriter bwHijo = new BufferedWriter(new OutputStreamWriter(pr.getOutputStream()));
            Scanner inputHijo = new Scanner(pr.getInputStream());
            if(ficheroTraducir != null){
                FileReader fr = new FileReader(ficheroTraducir);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                while ((linea=br.readLine())!= null){
                    bwHijo.write(linea);
                    bwHijo.newLine();
                    bwHijo.flush();
                    bw.write(linea+" - >> "+inputHijo.nextLine());
                    bw.newLine();
                    bw.flush();
                }
                br.close();
            } else{
                Scanner input = new Scanner(System.in);
                String linea;
                while (!(linea=input.nextLine()).equals("1")){
                    bwHijo.write(linea);
                    bwHijo.newLine();
                    bwHijo.flush();
                    String salidaHijo = inputHijo.nextLine();
                    System.out.println(salidaHijo);
                    bw.write(linea+" - >> "+salidaHijo);
                    bw.newLine();
                    bw.flush();
                }
                input.close();
            }
            bw.close();
            bwHijo.close();
            inputHijo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
