package Ejercicio4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Traductor {
    private static HashMap<String, String> diccionario = new HashMap<>();
    private static ArrayList<String> diccInterno = new ArrayList<>();
    public static void main(String[] args) {
        rellenarDiccionario();
        FileReader dicc;
        BufferedReader brDiccionario;

        if(args.length>1){
            if(args[0].equals("-d")){
                try {
                    dicc = new FileReader(args[1]);
                    brDiccionario = new BufferedReader(dicc);
                    leerDiccionario(brDiccionario);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Argumentos no válidos");
            }
        } else{
            try {
                leerDiccInterno();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Scanner input = new Scanner(System.in);
        String linea;
        while(!(linea = input.nextLine()).equals("1")){
            String traducido = (diccionario.get(linea)!=null)?diccionario.get(linea):"desconocido";
            System.out.println(traducido);
        }
    }

    private static void rellenarDiccionario() {
        diccInterno.add("cosas-things");
        diccInterno.add("casa-house");
        diccInterno.add("casas-houses");
    }

    private static void leerDiccionario(BufferedReader brDiccionario) throws IOException {
        String linea;
        while ((linea = brDiccionario.readLine()) != null){
            String[] traduccion = linea.split("-");
            diccionario.put(traduccion[0],traduccion[1]);
        }
    }

    private static void leerDiccInterno() throws IOException {
        for (int i = 0; i < diccInterno.size(); i++) {
            String[] traduccion = diccInterno.get(i).split("-");
            diccionario.put(traduccion[0],traduccion[1]);
        }
    }
}
