package Ejercicio5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Adder {
    public static void main(String[] args) {
        if(args.length<1){
            System.out.println("Introduce algún argumento");
            System.exit(1);
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(args[0]));
            int suma = br.lines().mapToInt(Integer::valueOf).sum();
            System.out.println(suma);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e){
            System.out.println("Error: "+e.getMessage());
        }
    }
}
