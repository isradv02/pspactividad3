package Ejercicio5;

import java.io.*;
import java.util.Scanner;

public class Ejercicio5 {
    public static void main(String[] args) {
        if(args.length<1){
            System.out.println("Introduce algún argumento");
            System.exit(1);
        }
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("Totals.txt"));
        int sumaFinal = 0;
        for (String arg:args) {
                String command = "java -jar Adder.jar "+arg;
                Process pr = new ProcessBuilder(command.split(" ")).start();
                Scanner output = new Scanner(pr.getInputStream());
                String valor = output.nextLine();
                sumaFinal += Integer.parseInt(valor);
                System.out.println(arg+": "+valor);
                bw.write(arg+": "+valor);
                bw.newLine();
                output.close();
        }
        System.out.println("Total: "+sumaFinal);
        bw.write("Total: "+sumaFinal);
        bw.close();
        System.out.println("Resultados almacenados en Totals.txt");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            System.out.println("Error: "+e.getMessage());
        }
    }
}
